package com.modanisa.streams.model;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.List;

public class ProductMultiLanguage {

    public static final Schema PRODUCT_MULTI_LANGUAGE_SCHEMA = SchemaBuilder.record("ProductMultiLanguage")
            .fields()
            .requiredString("name")
            .requiredString("slug")
            .requiredString("category")
            .optionalString("parentCategory")
            .name("fabrics").type().array().items().nullable().stringType().noDefault()
            .name("colors").type().array().items().nullable().stringType().noDefault()
            .endRecord();


    public GenericRecord createRecord(String name, String slug, String category, String parentCategory, List<String> colors, List<String> fabrics) {
        var record = new GenericData.Record(PRODUCT_MULTI_LANGUAGE_SCHEMA);
        record.put("name", name);
        record.put("slug", slug);
        record.put("category", category);
        record.put("parentCategory", parentCategory);
        record.put("colors", colors);
        record.put("fabrics", fabrics);
        return record;
    }
}
