package com.modanisa.streams.model;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

public class ProductImage {
    public static final Schema PRODUCT_IMAGE_SCHEMA = SchemaBuilder.record("ProductImage")
            .fields()
            .name("name").type().stringType().noDefault()
            .name("sort").type().intType().noDefault()
            .name("isGhost").type().booleanType().noDefault()
            .name("isOpen").type().booleanType().noDefault()
            .endRecord();

    public static GenericRecord createRecord(String name, int sort, boolean isGhost, boolean isOpen) {
        var record = new GenericData.Record(PRODUCT_IMAGE_SCHEMA);
        record.put("name", name);
        record.put("sort", sort);
        record.put("isGhost", isGhost);
        record.put("isOpen", isOpen);
        return record;
    }
}
