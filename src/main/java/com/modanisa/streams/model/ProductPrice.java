package com.modanisa.streams.model;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

public class ProductPrice {

    public static final Schema PRODUCT_PRICE_SCHEMA = SchemaBuilder.record("ProductPrice")
            .fields()
            .requiredFloat("price")
            .requiredFloat("priceAlt")
            .requiredString("currency")
            .endRecord();

    public static GenericRecord createRecord(Float price, Float priceAlt, String currency) {
        var record = new GenericData.Record(PRODUCT_PRICE_SCHEMA);
        record.put("price", price);
        record.put("priceAlt", priceAlt);
        record.put("currency", currency);
        return record;
    }
}
