package com.modanisa.streams.model;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.List;
import java.util.Map;

import static com.modanisa.streams.model.ProductImage.PRODUCT_IMAGE_SCHEMA;
import static com.modanisa.streams.model.ProductMultiLanguage.PRODUCT_MULTI_LANGUAGE_SCHEMA;
import static com.modanisa.streams.model.ProductPrice.PRODUCT_PRICE_SCHEMA;

public class Product {

    private static final Schema PRODUCT_SCHEMA = SchemaBuilder.record("Product")
            .fields()
            .name("supplierId").type().stringType().noDefault()
            .name("brand").type().stringType().noDefault()
            .name("barcodes").type().array().items().nullable().stringType().noDefault()
            .name("tr").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("en").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("fr").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("de").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("ar").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("fa").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("id").type(PRODUCT_MULTI_LANGUAGE_SCHEMA).noDefault()
            .name("prices").type().map().values(PRODUCT_PRICE_SCHEMA).noDefault()
            .name("productImages").type().array().items(PRODUCT_IMAGE_SCHEMA).noDefault()
            .endRecord();

    public GenericRecord createRecord(String brand, String supplierId, List<String> barcodes, GenericRecord languageTr, GenericRecord languageEn
            , GenericRecord languageFr, GenericRecord languageDe, GenericRecord languageAr, GenericRecord languageFa, GenericRecord languageId,
                                      Map<String, GenericRecord> prices, List<GenericRecord> productImages) {
        var record = new GenericData.Record(PRODUCT_SCHEMA);
        record.put("supplierId", supplierId);
        record.put("brand", brand);
        record.put("barcodes", barcodes);
        record.put("tr", languageTr);
        record.put("en", languageEn);
        record.put("fr", languageFr);
        record.put("de", languageDe);
        record.put("ar", languageAr);
        record.put("fa", languageFa);
        record.put("id", languageId);
        record.put("prices", prices);
        record.put("productImages", productImages);
        return record;
    }
}
