package com.modanisa.streams.model;

import lombok.AllArgsConstructor;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@AllArgsConstructor
public enum ProductProperty {

    COLOR(23L) {
        @Override
        public GenericRecord setPropertiesByLanguage(GenericRecord joinModel, GenericRecord optionValue) {
            if (((List<Utf8>) joinModel.get("colorsTr")).contains(new Utf8(Optional.ofNullable(optionValue.get("propertyNameTr")).orElse("").toString()))){
                return joinModel;
            }
            ((List<String>) joinModel.get("colorsTr")).add(Optional.ofNullable(optionValue.get("propertyNameTr")).orElse("").toString());
            ((List<String>) joinModel.get("colorsEn")).add(Optional.ofNullable(optionValue.get("propertyNameEn")).orElse("").toString());
            ((List<String>) joinModel.get("colorsFr")).add(Optional.ofNullable(optionValue.get("propertyNameFr")).orElse("").toString());
            ((List<String>) joinModel.get("colorsDe")).add(Optional.ofNullable(optionValue.get("propertyNameDe")).orElse("").toString());
            ((List<String>) joinModel.get("colorsAr")).add(Optional.ofNullable(optionValue.get("propertyNameAr")).orElse("").toString());
            ((List<String>) joinModel.get("colorsFa")).add(Optional.ofNullable(optionValue.get("propertyNameFa")).orElse("").toString());
            ((List<String>) joinModel.get("colorsId")).add(Optional.ofNullable(optionValue.get("propertyNameId")).orElse("").toString());
            return joinModel;
        }
    },
    FABRIC(21L) {
        @Override
        public GenericRecord setPropertiesByLanguage(GenericRecord joinModel, GenericRecord optionValue) {
            if (((List<Utf8>) joinModel.get("fabricsTr")).contains(new Utf8(Optional.ofNullable(optionValue.get("propertyNameTr")).orElse("").toString()))){
                return joinModel;
            }
            ((List<String>) joinModel.get("fabricsTr")).add(Optional.ofNullable(optionValue.get("propertyNameTr")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsEn")).add(Optional.ofNullable(optionValue.get("propertyNameEn")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsFr")).add(Optional.ofNullable(optionValue.get("propertyNameFr")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsDe")).add(Optional.ofNullable(optionValue.get("propertyNameDe")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsAr")).add(Optional.ofNullable(optionValue.get("propertyNameAr")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsFa")).add(Optional.ofNullable(optionValue.get("propertyNameFa")).orElse("").toString());
            ((List<String>) joinModel.get("fabricsId")).add(Optional.ofNullable(optionValue.get("propertyNameId")).orElse("").toString());
            return joinModel;
        }
    };

    private final Long id;

    public abstract GenericRecord setPropertiesByLanguage(GenericRecord joinModel, GenericRecord optionValue);

    public static ProductProperty fromId(Long id) {
        return Stream.of(ProductProperty.values())
                .filter((property -> property.id.equals(id)))
                .findAny()
                .orElse(null);
    }
}
