package com.modanisa.streams;

import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Properties;

public class KafkaProperties {
    private static final String BOOTSTRAP_SERVERS = "b-1.sherlock-debezium-kafk.nfrrm3.c1.kafka.eu-west-1.amazonaws.com:9092,b-2.sherlock-debezium-kafk.nfrrm3.c1.kafka.eu-west-1.amazonaws.com:9092";
    static final String SCHEMA_REGISTRY_URL = "http://confluent-cp-schema-registry.confluent.svc.cluster.local:8081";
    static final String PRODUCT_BRANDS_TOPIC = "mdns.modanisa.product_brands";
    static final String CATEGORY_TOPIC = "mdns.modanisa.category";
    static final String PRODUCTS_TOPIC = "mdns.modanisa.products";
    static final String PRODUCT_PROPERTIES_TOPIC = "mdns.modanisa.product_properties";
    static final String PRODUCT_OPTION_VALUES_TOPIC = "mdns.modanisa.product_option_values";
    static final String PRODUCT_VARIANTS_TOPIC = "mdns.modanisa.product_variants";
    static final String PRODUCT_IMAGES_TOPIC = "mdns.modanisa.product_images";
    static final String COUNTRY_IMAGES = "mdns.modanisa.product_gallery_images";
    static final String COUNTRY_IMAGE_MAPPING = "mdns.modanisa.image_gallery_countries";
    static final String COUNTRY_PRICE_TOPIC = "mdns.modanisa.country_price";
    static final String COUNTRY_CODE_TOPIC = "mdns.modanisa.static_country_list";

    static Properties getProperties() {
        final var props = new Properties();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, RandomStringUtils.randomAlphabetic(10));
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, 15 * 1024 * 1024);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, RandomStringUtils.randomAlphabetic(10));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.Long().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
        props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(StreamsConfig.TOPOLOGY_OPTIMIZATION, StreamsConfig.OPTIMIZE);
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 2);
        props.put(StreamsConfig.topicPrefix(TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG),2);
        props.put(StreamsConfig.producerPrefix(ProducerConfig.ACKS_CONFIG), "all");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 15 * 1024 * 1024L);
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 1000);
        props.put(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, SCHEMA_REGISTRY_URL);
        return props;
    }
}
