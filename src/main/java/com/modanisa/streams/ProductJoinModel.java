package com.modanisa.streams;

import com.modanisa.streams.model.ProductImage;
import com.modanisa.streams.model.ProductPrice;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import java.util.*;

public class ProductJoinModel {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("TestProductModel")
            .fields()
            .optionalLong("productId")
            .optionalString("supplierId")
            .optionalLong("categoryId")
            .optionalLong("parentCategoryId")
            .requiredString("productNameTr")
            .requiredString("productNameEn")
            .requiredString("productNameFr")
            .requiredString("productNameDe")
            .requiredString("productNameAr")
            .requiredString("productNameFa")
            .requiredString("productNameId")
            .optionalString("categoryNameTr")
            .optionalString("categoryNameEn")
            .optionalString("categoryNameFr")
            .optionalString("categoryNameDe")
            .optionalString("categoryNameAr")
            .optionalString("categoryNameFa")
            .optionalString("categoryNameId")
            .optionalString("parentCategoryNameTr")
            .optionalString("parentCategoryNameEn")
            .optionalString("parentCategoryNameFr")
            .optionalString("parentCategoryNameDe")
            .optionalString("parentCategoryNameAr")
            .optionalString("parentCategoryNameFa")
            .optionalString("parentCategoryNameId")
            /*.optionalLong("valueId")
            .optionalLong("optionId")
            .optionalString("propertyNameTr")
            .optionalString("propertyNameEn")
            .optionalString("propertyNameFr")
            .optionalString("propertyNameDe")
            .optionalString("propertyNameAr")
            .optionalString("propertyNameFa")
            .optionalString("propertyNameId")
            /*.name("colorsTr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsEn").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsFr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsDe").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsAr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsFa").type().nullable().array().items().nullable().stringType().noDefault()
            .name("colorsId").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsTr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsEn").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsFr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsDe").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsAr").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsFa").type().nullable().array().items().nullable().stringType().noDefault()
            .name("fabricsId").type().nullable().array().items().nullable().stringType().noDefault()
            .name("barcodes").type().nullable().array().items().nullable().stringType().noDefault()
            .optionalString("barcode")*/
            .optionalLong("brandId")
            .optionalString("brandName")
            .optionalString("slugTr")
            .optionalString("slugEn")
            .optionalString("slugFr")
            .optionalString("slugDe")
            .optionalString("slugAr")
            .optionalString("slugFa")
            .optionalString("slugId")
            //.optionalLong("countryId")
            //.optionalLong("countryIdInImage")
            //.optionalString("countryCode")
            //.optionalFloat("price")
            //.optionalFloat("priceAlt")
            //.optionalString("currency")
            .name("prices").type().map().values(ProductPrice.PRODUCT_PRICE_SCHEMA).noDefault()
            .optionalString("picFolder")
            /*.name("imageGalleryId").type().nullable().longType().noDefault()
            .name("productImages").type().nullable().array().items(ProductImage.PRODUCT_IMAGE_SCHEMA).noDefault()
            .name("countryImages").type().nullable().array().items().nullable().stringType().noDefault()*/
            .name("mainImage").type().stringType().noDefault()
            .name("mainImageIsGhost").type().intType().noDefault()
            /*.name("productImage").type().nullable().stringType().noDefault()
            .name("countryImage").type().nullable().stringType().noDefault()
            .name("productImageSort").type().nullable().intType().noDefault()
            .name("countryImageSort").type().nullable().intType().noDefault()
            .name("productImageIsGhost").type().nullable().intType().noDefault()
            .name("isOpen").type().nullable().intType().noDefault()*/
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);
        record.put("supplierId", "");
        record.put("productNameTr", "");
        record.put("productNameEn", "");
        record.put("productNameFr", "");
        record.put("productNameDe", "");
        record.put("productNameAr", "");
        record.put("productNameFa", "");
        record.put("productNameId", "");

        record.put("categoryId", 0L);
        record.put("categoryNameTr", "");
        record.put("categoryNameEn", "");
        record.put("categoryNameFr", "");
        record.put("categoryNameDe", "");
        record.put("categoryNameAr", "");
        record.put("categoryNameFa", "");
        record.put("categoryNameId", "");

        record.put("parentCategoryId", 0L);
        record.put("parentCategoryNameTr", "");
        record.put("parentCategoryNameEn", "");
        record.put("parentCategoryNameFr", "");
        record.put("parentCategoryNameDe", "");
        record.put("parentCategoryNameAr", "");
        record.put("parentCategoryNameFa", "");
        record.put("parentCategoryNameId", "");

        record.put("valueId", 0L);
        record.put("optionId", 0L);

        record.put("propertyNameTr", "");
        record.put("propertyNameEn", "");
        record.put("propertyNameFr", "");
        record.put("propertyNameDe", "");
        record.put("propertyNameAr", "");
        record.put("propertyNameFa", "");
        record.put("propertyNameId", "");

        record.put("colorsTr", new ArrayList<String>());
        record.put("colorsEn", new ArrayList<String>());
        record.put("colorsFr", new ArrayList<String>());
        record.put("colorsDe", new ArrayList<String>());
        record.put("colorsAr", new ArrayList<String>());
        record.put("colorsFa", new ArrayList<String>());
        record.put("colorsId", new ArrayList<String>());

        record.put("fabricsTr", new ArrayList<String>());
        record.put("fabricsEn", new ArrayList<String>());
        record.put("fabricsFr", new ArrayList<String>());
        record.put("fabricsDe", new ArrayList<String>());
        record.put("fabricsAr", new ArrayList<String>());
        record.put("fabricsFa", new ArrayList<String>());
        record.put("fabricsId", new ArrayList<String>());

        record.put("barcodes", new ArrayList<String>());
        record.put("barcode", "");

        record.put("brandId", 0L);
        record.put("brandName", "");

        record.put("slugTr", "");
        record.put("slugEn", "");
        record.put("slugFr", "");
        record.put("slugDe", "");
        record.put("slugAr", "");
        record.put("slugFa", "");
        record.put("slugId", "");

        record.put("picFolder", "");
        record.put("countryId", 0L);
        record.put("countryIdInImage", 0L);
        record.put("countryCode", "");
        record.put("price", 0F);
        record.put("priceAlt", 0F);
        record.put("currency", "");
        record.put("prices", new HashMap<String, GenericRecord>());

        record.put("imageGalleryId", 0L);
        record.put("productImages", new ArrayList<String>());
        record.put("countryImages", new ArrayList<String>());
        record.put("mainImage", "");
        record.put("mainImageIsGhost", 0);
        record.put("countryImage", "");
        record.put("productImage", "");
        record.put("productImageSort", 0);
        record.put("countryImageSort", 0);
        record.put("productImageIsGhost", 0);
        record.put("isOpen", 0);
        return record;
    }

    public static GenericRecord createEmptyRecordForProducts() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);
        record.put("supplierId", "");
        record.put("productNameTr", "");
        record.put("productNameEn", "");
        record.put("productNameFr", "");
        record.put("productNameDe", "");
        record.put("productNameAr", "");
        record.put("productNameFa", "");
        record.put("productNameId", "");

        record.put("brandId", 0L);
        record.put("categoryId", 0L);

        record.put("categoryId", 0L);
        record.put("categoryNameTr", "");
        record.put("categoryNameEn", "");
        record.put("categoryNameFr", "");
        record.put("categoryNameDe", "");
        record.put("categoryNameAr", "");
        record.put("categoryNameFa", "");
        record.put("categoryNameId", "");

        record.put("parentCategoryId", 0L);
        record.put("parentCategoryNameTr", "");
        record.put("parentCategoryNameEn", "");
        record.put("parentCategoryNameFr", "");
        record.put("parentCategoryNameDe", "");
        record.put("parentCategoryNameAr", "");
        record.put("parentCategoryNameFa", "");
        record.put("parentCategoryNameId", "");

        record.put("brandName", "");

        record.put("slugTr", "");
        record.put("slugEn", "");
        record.put("slugFr", "");
        record.put("slugDe", "");
        record.put("slugAr", "");
        record.put("slugFa", "");
        record.put("slugId", "");

        record.put("picFolder", "");
        record.put("mainImage", "");
        record.put("mainImageIsGhost", 0);

        record.put("prices", new HashMap<String, GenericRecord>());

        return record;
    }

    public static GenericRecord createEmptyRecordForBarcodes() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);
        record.put("barcodes", new ArrayList<String>());
        return record;
    }
}
