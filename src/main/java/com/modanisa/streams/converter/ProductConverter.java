package com.modanisa.streams.converter;

import com.modanisa.streams.model.Product;
import com.modanisa.streams.model.ProductImage;
import com.modanisa.streams.model.ProductMultiLanguage;
import org.apache.avro.generic.GenericRecord;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductConverter {

    private final Product product = new Product();
    private final ProductMultiLanguage productMultiLanguage = new ProductMultiLanguage();

    public GenericRecord convert(GenericRecord joinModel) {
        var brand = joinModel.get("brandName").toString();
        System.out.print(joinModel.get("supplierId"));
        var supplierId   = Objects.isNull(joinModel.get("supplierId")) ? "" : joinModel.get("supplierId").toString();
        var tr = createMultiLanguageTr(joinModel);
        var en = createMultiLanguageEn(joinModel);
        var fr = createMultiLanguageFr(joinModel);
        var de = createMultiLanguageDe(joinModel);
        var ar = createMultiLanguageAr(joinModel);
        var fa = createMultiLanguageFa(joinModel);
        var id = createMultiLanguageId(joinModel);
        var barcodes = ((List<String>) joinModel.get("barcodes")).stream().distinct().collect(Collectors.toList());
        var prices = (Map<String, GenericRecord>) joinModel.get("prices");

        var productImage = new ProductImage();
        var images = (List<GenericRecord>) joinModel.get("productImages");
        var mainImageIsGhost = Integer.parseInt(joinModel.get("mainImageIsGhost").toString()) != 0;
        var name = joinModel.get("picFolder").toString() + joinModel.get("mainImage").toString();
        var mainImage = productImage.createRecord(name, 0, mainImageIsGhost, true);
        images.add(mainImage);

        return product.createRecord(brand, supplierId, barcodes, tr, en, fr, de, ar, fa, id, prices, images);
    }

    private GenericRecord createMultiLanguageTr(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameTr")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugTr")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameTr")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameTr")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsTr")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsTr")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageEn(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameEn")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugEn")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameEn")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameEn")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsEn")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsEn")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageFr(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameFr")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugFr")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameFr")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameFr")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsFr")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsFr")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageDe(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameDe")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugDe")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameDe")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameDe")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsDe")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsDe")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageAr(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameAr")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugAr")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameAr")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameAr")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsAr")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsAr")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageFa(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameFa")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugFa")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameFa")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameFa")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsFa")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsFa")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }

    private GenericRecord createMultiLanguageId(GenericRecord joinModel) {
        var name = Optional.ofNullable(joinModel.get("productNameId")).orElse("").toString();
        var slug = Optional.ofNullable(joinModel.get("slugId")).orElse("").toString();
        var category = Optional.ofNullable(joinModel.get("categoryNameId")).orElse("").toString();
        var parentCategory = Optional.ofNullable(joinModel.get("parentCategoryNameId")).orElse("").toString();
        var colors = ((List<String>) joinModel.get("colorsId")).stream().distinct().collect(Collectors.toList());
        var fabrics = ((List<String>) joinModel.get("fabricsId")).stream().distinct().collect(Collectors.toList());

        return productMultiLanguage.createRecord(name, slug, category, parentCategory, colors, fabrics);
    }
}
