package com.modanisa.streams;

import com.modanisa.streams.model.ProductImage;
import com.modanisa.streams.model.ProductPrice;
import com.modanisa.streams.model.ProductProperty;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.util.Utf8;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Aggregator {

    public GenericRecord aggregate(GenericRecord joinModel, GenericRecord value) {
        joinModel.put("productId", Long.parseLong(value.get("productId").toString()));
        joinModel.put("categoryId", Long.parseLong(value.get("categoryId").toString()));
        joinModel.put("brandName", value.get("brandName").toString());
        setProductName(joinModel, value);
        setCategoryIdAndName(joinModel, value);
        setSlug(joinModel, value);
        var barcodes = (List<String>) joinModel.get("barcodes");
        var barcode = value.get("barcode").toString();
        barcodes.add(barcode);

        setProductImage(joinModel, value);
        setPrice(joinModel, value);

        var optionId = Long.parseLong(value.get("optionId").toString());
        var requiredProperty = ProductProperty.fromId(optionId);
        if (Objects.nonNull(requiredProperty)) {
            requiredProperty.setPropertiesByLanguage(joinModel, value);
        }
        return joinModel;
    }

    private void setProductImage(GenericRecord joinModel, GenericRecord value) {
        var productImage = new ProductImage();
        var productImages = (List<GenericRecord>) joinModel.get("productImages");
        var mainImageIsGhost = Integer.parseInt(value.get("mainImageIsGhost").toString()) != 0;
        var mainImage = productImage.createRecord(value.get("mainImage").toString(), 0, mainImageIsGhost, true);
        var productImageSort = Integer.parseInt(value.get("productImageSort").toString());
        var productImageIsGhost = Integer.parseInt(value.get("productImageIsGhost").toString()) != 0;
        var isOpen = Integer.parseInt(value.get("isOpen").toString()) != 0;
        var image = productImage.createRecord(value.get("productImage").toString(), productImageSort + 1, productImageIsGhost, isOpen);
        productImages.add(mainImage);
        productImages.add(image);
    }

    private void setPrice(GenericRecord joinModel, GenericRecord value) {
        var countryCode = value.get("countryCode").toString();
        var price = Float.valueOf(value.get("price").toString());
        var priceAlt = Float.valueOf(value.get("priceAlt").toString());
        var currency = value.get("currency").toString();

        // get price in TL from value(product) and set it to join/aggregate model
        var priceInTL = ((Map<String, GenericRecord>) value.get("prices")).get(new Utf8("TR"));
        ((Map<String, GenericRecord>) joinModel.get("prices")).put("TR", priceInTL);

        // get price info from value(country_price) and set it to join/aggregate model
        var record = new ProductPrice().createRecord(price, priceAlt, currency);
        ((Map<String, GenericRecord>) joinModel.get("prices")).put(countryCode, record);
    }

    private void setSlug(GenericRecord joinModel, GenericRecord value) {
        joinModel.put("slugTr", value.get("slugTr"));
        joinModel.put("slugEn", value.get("slugEn"));
        joinModel.put("slugFr", value.get("slugFr"));
        joinModel.put("slugDe", value.get("slugDe"));
        joinModel.put("slugAr", value.get("slugAr"));
        joinModel.put("slugFa", value.get("slugFa"));
        joinModel.put("slugId", value.get("slugId"));
    }

    private void setProductName(GenericRecord joinModel, GenericRecord value) {
        joinModel.put("productNameTr", Optional.ofNullable(value.get("productNameTr")).orElse(""));
        joinModel.put("productNameEn", Optional.ofNullable(value.get("productNameEn")).orElse(""));
        joinModel.put("productNameFr", Optional.ofNullable(value.get("productNameFr")).orElse(""));
        joinModel.put("productNameDe", Optional.ofNullable(value.get("productNameDe")).orElse(""));
        joinModel.put("productNameAr", Optional.ofNullable(value.get("productNameAr")).orElse(""));
        joinModel.put("productNameFa", Optional.ofNullable(value.get("productNameFa")).orElse(""));
        joinModel.put("productNameId", Optional.ofNullable(value.get("productNameId")).orElse(""));
    }

    private void setCategoryIdAndName(GenericRecord joinModel, GenericRecord value) {
        joinModel.put("categoryId", Long.parseLong(value.get("categoryId").toString()));
        joinModel.put("categoryNameTr", value.get("categoryNameTr").toString());
        joinModel.put("categoryNameEn", value.get("categoryNameEn").toString());
        joinModel.put("categoryNameFr", value.get("categoryNameFr").toString());
        joinModel.put("categoryNameDe", value.get("categoryNameDe").toString());
        joinModel.put("categoryNameAr", value.get("categoryNameAr").toString());
        joinModel.put("categoryNameFa", value.get("categoryNameFa").toString());
        joinModel.put("categoryNameId", value.get("categoryNameId").toString());

        joinModel.put("parentCategoryId", Long.parseLong(value.get("parentCategoryId").toString()));
        joinModel.put("parentCategoryNameTr", value.get("parentCategoryNameTr").toString());
        joinModel.put("parentCategoryNameEn", value.get("parentCategoryNameEn").toString());
        joinModel.put("parentCategoryNameFr", value.get("parentCategoryNameFr").toString());
        joinModel.put("parentCategoryNameDe", value.get("parentCategoryNameDe").toString());
        joinModel.put("parentCategoryNameAr", value.get("parentCategoryNameAr").toString());
        joinModel.put("parentCategoryNameFa", value.get("parentCategoryNameFa").toString());
        joinModel.put("parentCategoryNameId", value.get("parentCategoryNameId").toString());
    }
}
