package com.modanisa.streams;

import com.modanisa.streams.model.ProductPrice;
import org.apache.avro.generic.GenericRecord;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class Joiner {

    public GenericRecord setProductAttributes(GenericRecord joinModel, GenericRecord product) {
        joinModel.put("productId", Long.parseLong(product.get("id").toString()));
        joinModel.put("supplierId", Objects.isNull(product.get("supplier_id")) ? "" : product.get("supplier_id").toString());
        joinModel.put("productNameTr", product.get("name"));
        joinModel.put("productNameEn", Optional.ofNullable(product.get("name_en")).orElse(""));
        joinModel.put("productNameFr", Optional.ofNullable(product.get("name_fr")).orElse(""));
        joinModel.put("productNameDe", Optional.ofNullable(product.get("name_de")).orElse(""));
        joinModel.put("productNameAr", Optional.ofNullable(product.get("name_ar")).orElse(""));
        joinModel.put("productNameFa", Optional.ofNullable(product.get("name_fa")).orElse(""));
        joinModel.put("productNameId", Optional.ofNullable(product.get("name_id")).orElse(""));
        joinModel.put("brandId", Long.parseLong(product.get("brand_id").toString()));
        joinModel.put("categoryId", Long.parseLong(product.get("mcategory").toString()));
        joinModel.put("slugTr", Optional.ofNullable(product.get("sef")).orElse("").toString());
        joinModel.put("slugEn", Optional.ofNullable(product.get("sef_en")).orElse("").toString());
        joinModel.put("slugFr", Optional.ofNullable(product.get("sef_fr")).orElse("").toString());
        joinModel.put("slugDe", Optional.ofNullable(product.get("sef_de")).orElse("").toString());
        joinModel.put("slugAr", Optional.ofNullable(product.get("sef_ar")).orElse("").toString());
        joinModel.put("slugFa", Optional.ofNullable(product.get("sef_fa")).orElse("").toString());
        joinModel.put("slugId", Optional.ofNullable(product.get("sef_id")).orElse("").toString());
        joinModel.put("picFolder", Optional.ofNullable(product.get("pic_folder")).orElse("").toString());
        joinModel.put("mainImage", Optional.ofNullable(product.get("pic")).orElse("").toString());
        joinModel.put("mainImageIsGhost", Optional.ofNullable(product.get("ghostImage")).orElse(0));
        //set TR price by using product.price and product.price_alt columns
        var prices = (Map<String, GenericRecord>) joinModel.get("prices");
        var price = Float.parseFloat(product.get("price").toString());
        var priceAlt = Float.parseFloat(product.get("price_alt").toString());

        var priceInTL = new ProductPrice().createRecord(price, priceAlt, "TL");
        prices.put("TR", priceInTL);
        return joinModel;
    }

    public GenericRecord setCategoryAttributes(GenericRecord joinModel, GenericRecord category) {
        joinModel.put("categoryId", Long.parseLong(category.get("id").toString()));
        joinModel.put("parentCategoryId", Long.parseLong(category.get("parent").toString()));
        joinModel.put("categoryNameTr", Optional.ofNullable(category.get("name")).orElse(""));
        joinModel.put("categoryNameEn", Optional.ofNullable(category.get("name_en")).orElse(""));
        joinModel.put("categoryNameFr", Optional.ofNullable(category.get("name_fr")).orElse(""));
        joinModel.put("categoryNameDe", Optional.ofNullable(category.get("name_de")).orElse(""));
        joinModel.put("categoryNameAr", Optional.ofNullable(category.get("name_ar")).orElse(""));
        joinModel.put("categoryNameFa", Optional.ofNullable(category.get("name_fa")).orElse(""));
        joinModel.put("categoryNameId", Optional.ofNullable(category.get("name_id")).orElse(""));
        return joinModel;
    }

    public GenericRecord setParentCategoryAttributes(GenericRecord joinModel, GenericRecord parentCategory) {
        joinModel.put("parentCategoryNameTr", Optional.ofNullable(parentCategory.get("name")).orElse(""));
        joinModel.put("parentCategoryNameEn", Optional.ofNullable(parentCategory.get("name_en")).orElse(""));
        joinModel.put("parentCategoryNameFr", Optional.ofNullable(parentCategory.get("name_fr")).orElse(""));
        joinModel.put("parentCategoryNameDe", Optional.ofNullable(parentCategory.get("name_de")).orElse(""));
        joinModel.put("parentCategoryNameAr", Optional.ofNullable(parentCategory.get("name_ar")).orElse(""));
        joinModel.put("parentCategoryNameFa", Optional.ofNullable(parentCategory.get("name_fa")).orElse(""));
        joinModel.put("parentCategoryNameId", Optional.ofNullable(parentCategory.get("name_id")).orElse(""));
        return joinModel;
    }

    public GenericRecord setOptionIdAndValueIdAttributes(GenericRecord productProperties, GenericRecord product) {
        product.put("valueId", Long.parseLong(productProperties.get("value_id").toString()));
        product.put("optionId", Long.parseLong(productProperties.get("option_id").toString()));
        return product;
    }

    public GenericRecord setProductOptionValues(GenericRecord joinModel, GenericRecord optionValue) {
        joinModel.put("propertyNameTr", Optional.ofNullable(optionValue.get("name")).orElse(""));
        joinModel.put("propertyNameEn", Optional.ofNullable(optionValue.get("name_en")).orElse(""));
        joinModel.put("propertyNameFr", Optional.ofNullable(optionValue.get("name_fr")).orElse(""));
        joinModel.put("propertyNameDe", Optional.ofNullable(optionValue.get("name_de")).orElse(""));
        joinModel.put("propertyNameAr", Optional.ofNullable(optionValue.get("name_ar")).orElse(""));
        joinModel.put("propertyNameFa", Optional.ofNullable(optionValue.get("name_fa")).orElse(""));
        joinModel.put("propertyNameId", Optional.ofNullable(optionValue.get("name_id")).orElse(""));
        return joinModel;
    }

    public GenericRecord setProductBarcode(GenericRecord joinModel, GenericRecord variant) {
        joinModel.put("barcode", variant.get("ean").toString());
        return joinModel;
    }

    public GenericRecord setProductImage(GenericRecord joinModel, GenericRecord images) {
        return joinModel;
    }

    public GenericRecord setCountryImage(GenericRecord joinModel, GenericRecord images) {
        joinModel.put("imageGalleryId", Long.parseLong(images.get("image_gallery_id").toString()));
        joinModel.put("countryImage", images.get("image").toString());
        joinModel.put("countryImageSort", Integer.parseInt(images.get("sort").toString()));
        return joinModel;
    }

    public GenericRecord setBrandName(GenericRecord joinModel, GenericRecord brand) {
        joinModel.put("brandName", Optional.ofNullable(brand.get("name")).orElse(""));
        return joinModel;
    }

    public GenericRecord setPrice(GenericRecord joinModel, GenericRecord countryPrice) {
        joinModel.put("countryId", Long.parseLong(countryPrice.get("COUNTRY_ID").toString()));
        joinModel.put("price", Float.valueOf(countryPrice.get("PRICE").toString()));
        joinModel.put("priceAlt", Float.valueOf(countryPrice.get("PRICE_ALT").toString()));
        joinModel.put("currency", countryPrice.get("CURRENCY").toString());
        return joinModel;
    }

    public GenericRecord setCountryCode(GenericRecord joinModel, GenericRecord country) {
        joinModel.put("countryCode", Optional.ofNullable(country.get("code")).orElse("").toString());
        return joinModel;
    }
}
