package com.modanisa.streams;

import com.modanisa.streams.converter.ProductConverter;
import com.modanisa.streams.model.ProductImage;
import com.modanisa.streams.model.ProductPrice;
import com.modanisa.streams.model.ProductProperty;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.streams.serdes.avro.GenericAvroSerde;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.*;
import java.util.function.Function;

public class Main {

    public static void main(final String[] args) {
        var keyAvroSerde = new GenericAvroSerde();
        var valueAvroSerde = new GenericAvroSerde();
        keyAvroSerde.configure(Collections.singletonMap(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY_URL), true);
        valueAvroSerde.configure(Collections.singletonMap(KafkaAvroDeserializerConfig.SCHEMA_REGISTRY_URL_CONFIG, KafkaProperties.SCHEMA_REGISTRY_URL), false);
        long startTime = System.currentTimeMillis();
        var streamBuilder = new StreamsBuilder();

        var joiner = new Joiner();
        var productConverter = new ProductConverter();

        var categoryTable =  streamBuilder
                .stream(KafkaProperties.CATEGORY_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("category_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        var productTable =  streamBuilder
                .stream(KafkaProperties.PRODUCTS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .mapValues((categoryId, product) -> joiner.setProductAttributes(ProductJoinModel.createEmptyRecordForProducts(), product))
                .selectKey((key, value) -> Long.parseLong(value.get("productId").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("product_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        Function<GenericRecord, Long> foreignKeyExtractorForCategory = (x) -> Long.parseLong(x.get("categoryId").toString());
        var withCategory = productTable
                .join(categoryTable, foreignKeyExtractorForCategory, joiner::setCategoryAttributes);

        Function<GenericRecord, Long> foreignKeyExtractorForParentCategory = (x) -> Long.parseLong(x.get("parentCategoryId").toString());
        var withParentCategory = withCategory
                .join(categoryTable, foreignKeyExtractorForParentCategory, joiner::setParentCategoryAttributes);

        var brandTable =  streamBuilder
                .stream(KafkaProperties.PRODUCT_BRANDS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("brand_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        Function<GenericRecord, Long> foreignKeyExtractorForBrand = (x) -> Long.parseLong(x.get("brandId").toString());
        withParentCategory.join(brandTable, foreignKeyExtractorForBrand, joiner::setBrandName)
                .toStream()
                .to("products");

        streamBuilder
                .stream(KafkaProperties.PRODUCT_VARIANTS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("product_id").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductBarcode::createEmptyRecord, (key, value, aggregate) -> {
                    aggregate.put("productId", Long.parseLong(value.get("product_id").toString()));
                    List<String> list =  (List<String>)aggregate.get("barcodes");
                    list.add(value.get("ean").toString());
                    return aggregate;
                })
                .toStream()
                .to("products");

        streamBuilder
                .stream(KafkaProperties.PRODUCT_IMAGES_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("product").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductImages::createEmptyRecord, (key, value, aggregate) -> {
                    aggregate.put("productId", Long.parseLong(value.get("product").toString()));

                    GenericRecord productImage = ProductImage.createRecord(value.get("pic").toString(),
                            Integer.parseInt(value.get("sort").toString()),
                            Boolean.parseBoolean(value.get("ghost_image").toString()),
                            Boolean.parseBoolean(value.get("is_open_image").toString())
                    );

                    List list =  (List<String>)aggregate.get("productImages");
                    list.add(productImage);
                    return aggregate;
                })
                .toStream()
                .to("products");

        streamBuilder
                .stream(KafkaProperties.COUNTRY_PRICE_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("PRODUCT_ID").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductPrices::createEmptyRecord, (key, value, aggregate) -> {
                    aggregate.put("productId", Long.parseLong(value.get("PRODUCT_ID").toString()));
                    Map<String, GenericRecord> map =  (Map<String, GenericRecord>)aggregate.get("prices");
                    var price = Float.parseFloat(value.get("PRICE").toString());
                    var priceAlt = Float.parseFloat(value.get("PRICE_ALT").toString());
                    var prices = ProductPrice.createRecord(price, priceAlt, value.get("CURRENCY").toString());
                    map.put(value.get("COUNTRY_ID").toString(), prices);
                    return aggregate;
                })
                .toStream()
                .to("products");

//                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("price_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        /*var productOptionValue = streamBuilder
                .stream(KafkaProperties.PRODUCT_OPTION_VALUES_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("product_option_materialized").withKeySerde(org.apache.kafka.common.serialization.Serdes.Long()).withValueSerde(valueAvroSerde));

        var productPropertiesTable = streamBuilder
                .stream(KafkaProperties.PRODUCT_PROPERTIES_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .filter((key, value) -> value.get("option_id").toString().equals("21") || value.get("option_id").toString().equals("23"))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("product_properties_materialized").withKeySerde(org.apache.kafka.common.serialization.Serdes.Long()).withValueSerde(valueAvroSerde));

        Function<GenericRecord, Long> foreignKeyExtractor = (x) -> Long.parseLong(x.get("value_id").toString());
        var productPropertiesWithOptionValueTable = productPropertiesTable
                .join(productOptionValue, foreignKeyExtractor, (value1, value2) -> {
                    GenericRecord genericRecord = ProductPropertiesWithValue.createEmptyRecord();
                    genericRecord.put("productId", Long.parseLong(value1.get("product_id").toString()));
                    genericRecord.put("valueId", Long.parseLong(value1.get("value_id").toString()));
                    genericRecord.put("optionId", Long.parseLong(value1.get("option_id").toString()));
                    genericRecord.put("propertyNameEn", Optional.ofNullable(value2.get("name_en")).orElse(""));
                    genericRecord.put("propertyNameFr", Optional.ofNullable(value2.get("name_fr")).orElse(""));
                    genericRecord.put("propertyNameTr", Optional.ofNullable(value2.get("name")).orElse(""));
                    genericRecord.put("propertyNameAr", Optional.ofNullable(value2.get("name_ar")).orElse(""));
                    genericRecord.put("propertyNameFa", Optional.ofNullable(value2.get("name_fa")).orElse(""));
                    genericRecord.put("propertyNameId", Optional.ofNullable(value2.get("name_id")).orElse(""));
                    genericRecord.put("propertyNameDe", Optional.ofNullable(value2.get("name_de")).orElse(""));
                    return genericRecord;
                })
                .toStream()
                .selectKey((key, value) -> Long.parseLong(value.get("productId").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductPropertiesModel::createEmptyRecord, (key, value, aggregate) -> {
                    var requiredProperty = ProductProperty.fromId(Long.parseLong(value.get("optionId").toString()));
                    if (Objects.nonNull(requiredProperty)) {
                        requiredProperty.setPropertiesByLanguage(aggregate, value);
                    }
                    return aggregate;
                })
                .toStream()
                .toTable();

        var categoryTable =  streamBuilder
                .stream(KafkaProperties.CATEGORY_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("category_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        var productTable =  streamBuilder
                .stream(KafkaProperties.PRODUCTS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .filter((key, value) -> value.get("status").toString().equals("1"))
                .mapValues((categoryId, product) -> joiner.setProductAttributes(ProductJoinModel.createEmptyRecord(), product))
                .selectKey((key, value) -> Long.parseLong(value.get("productId").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("product_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        Function<GenericRecord, Long> foreignKeyExtractorForCategory = (x) -> Long.parseLong(x.get("categoryId").toString());
        var withCategory = productTable
                .join(categoryTable, foreignKeyExtractorForCategory, joiner::setCategoryAttributes)
                .toStream()
                .toTable();

        Function<GenericRecord, Long> foreignKeyExtractorForParentCategory = (x) -> Long.parseLong(x.get("parentCategoryId").toString());
        var withParentCategory = withCategory
                .join(categoryTable, foreignKeyExtractorForParentCategory, joiner::setParentCategoryAttributes)
                .toStream()
                .toTable();

        var brandTable =  streamBuilder
                .stream(KafkaProperties.PRODUCT_BRANDS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("id").toString()))
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("brand_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        Function<GenericRecord, Long> foreignKeyExtractorForBrand = (x) -> Long.parseLong(x.get("brandId").toString());
        var withBrand = withParentCategory.join(brandTable, foreignKeyExtractorForBrand, joiner::setBrandName)
                .toStream()
                .toTable();

        var productVariantsTable = streamBuilder
                .stream(KafkaProperties.PRODUCT_VARIANTS_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("product_id").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductBarcode::createEmptyRecord, (key, value, aggregate) -> {
                    aggregate.put("productId", Long.parseLong(value.get("product_id").toString()));
                    List<String> list =  (List<String>)aggregate.get("barcodes");
                    list.add(value.get("ean").toString());
                    aggregate.put("barcodes", list);
                    return aggregate;
                })
                .toStream()
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("barcodes_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        var withBarcodes = withBrand.join(productVariantsTable, (value1, value2) -> {
            value1.put("barcodes", value2.get("barcodes"));
            return value1;
        }).toStream().toTable();

        var withProductProperties = withBarcodes.join(productPropertiesWithOptionValueTable, (value1, value2) -> {
            value1.put("colorsTr", value2.get("colorsTr"));
            value1.put("colorsEn"  , value2.get("colorsEn"));
            value1.put("colorsFr"  , value2.get("colorsFr"));
            value1.put("colorsDe"  , value2.get("colorsDe"));
            value1.put("colorsAr"  , value2.get("colorsAr"));
            value1.put("colorsFa"  , value2.get("colorsFa"));
            value1.put("colorsId"  , value2.get("colorsId"));
            value1.put("fabricsTr" , value2.get("fabricsTr"));
            value1.put("fabricsEn" , value2.get("fabricsEn"));
            value1.put("fabricsFr" , value2.get("fabricsFr"));
            value1.put("fabricsDe" , value2.get("fabricsDe"));
            value1.put("fabricsAr" , value2.get("fabricsAr"));
            value1.put("fabricsFa" , value2.get("fabricsFa"));
            value1.put("fabricsId" , value2.get("fabricsId"));
            return value1;
        }).toStream().toTable();

        var priceTable = streamBuilder
                .stream(KafkaProperties.COUNTRY_PRICE_TOPIC, Consumed.with(keyAvroSerde, valueAvroSerde))
                .selectKey((key, value) -> Long.parseLong(value.get("PRODUCT_ID").toString()))
                .groupByKey(Grouped.with(Serdes.Long(), valueAvroSerde))
                .aggregate(ProductPrices::createEmptyRecord, (key, value, aggregate) -> {
                    aggregate.put("productId", Long.parseLong(value.get("PRODUCT_ID").toString()));
                    Map<String, GenericRecord> map =  (Map<String, GenericRecord>)aggregate.get("prices");
                    var price = Float.parseFloat(value.get("PRICE").toString());
                    var priceAlt = Float.parseFloat(value.get("PRICE_ALT").toString());
                    var prices = ProductPrice.createRecord(price, priceAlt, value.get("CURRENCY").toString());
                    map.put(value.get("COUNTRY_ID").toString(), prices);
                    aggregate.put("prices", map);
                    return aggregate;
                })
                .toStream()
                .toTable(Materialized.<Long, GenericRecord, KeyValueStore<Bytes, byte[]>>as("price_materialized").withKeySerde(Serdes.Long()).withValueSerde(valueAvroSerde));

        withProductProperties.leftJoin(priceTable, (value1, value2) -> {
            if(value2 != null){
                ((Map<String, GenericRecord>)value1.get("prices")).putAll((Map<String, GenericRecord>)value2.get("prices"));
            }
            return value1;
        }).toStream()
                .mapValues((readOnlyKey, value) -> productConverter.convert(value))
                .to("test");*/

        var topology = streamBuilder.build();
        System.out.println(topology.describe());

        var kafkaStreams = new KafkaStreams(topology, KafkaProperties.getProperties());
        kafkaStreams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println(System.currentTimeMillis() - startTime)));
    }
}

class ProductBarcode {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("TempProductWithBarcodeModel")
            .fields()
            .requiredLong("productId")
            .name("barcodes").type().array().items().nullable().stringType().noDefault()
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);

        record.put("barcodes", new ArrayList<String>());
        return record;
    }
}

class ProductPrices {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("Temp1ProductPricesModel")
            .fields()
            .requiredLong("productId")
            .name("prices").type().map().values(ProductPrice.PRODUCT_PRICE_SCHEMA).noDefault()
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);

        record.put("prices", new HashMap<String, GenericRecord>());
        return record;
    }
}

class ProductPropertiesWithValue {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("TempProductPropertiesWithValue")
            .fields()
            .optionalLong("productId")
            .optionalLong("valueId")
            .optionalLong("optionId")
            .optionalString("propertyNameTr")
            .optionalString("propertyNameEn")
            .optionalString("propertyNameFr")
            .optionalString("propertyNameDe")
            .optionalString("propertyNameAr")
            .optionalString("propertyNameFa")
            .optionalString("propertyNameId")
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);
        record.put("optionId", 0L);
        record.put("valueId", 0L);

        record.put("propertyNameEn", "");
        record.put("propertyNameTr", "");
        record.put("propertyNameFr", "");
        record.put("propertyNameDe", "");
        record.put("propertyNameAr", "");
        record.put("propertyNameFa", "");
        record.put("propertyNameId", "");

        return record;
    }
}

class ProductPropertiesModel {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("TempProductPropertiesModel")
            .fields()
            .optionalLong("productId")
            .name("colorsTr").type().array().items().nullable().stringType().noDefault()
            .name("colorsEn").type().array().items().nullable().stringType().noDefault()
            .name("colorsFr").type().array().items().nullable().stringType().noDefault()
            .name("colorsDe").type().array().items().nullable().stringType().noDefault()
            .name("colorsAr").type().array().items().nullable().stringType().noDefault()
            .name("colorsFa").type().array().items().nullable().stringType().noDefault()
            .name("colorsId").type().array().items().nullable().stringType().noDefault()
            .name("fabricsTr").type().array().items().nullable().stringType().noDefault()
            .name("fabricsEn").type().array().items().nullable().stringType().noDefault()
            .name("fabricsFr").type().array().items().nullable().stringType().noDefault()
            .name("fabricsDe").type().array().items().nullable().stringType().noDefault()
            .name("fabricsAr").type().array().items().nullable().stringType().noDefault()
            .name("fabricsFa").type().array().items().nullable().stringType().noDefault()
            .name("fabricsId").type().array().items().nullable().stringType().noDefault()
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);

        record.put("colorsTr", new ArrayList<String>());
        record.put("colorsEn", new ArrayList<String>());
        record.put("colorsFr", new ArrayList<String>());
        record.put("colorsDe", new ArrayList<String>());
        record.put("colorsAr", new ArrayList<String>());
        record.put("colorsFa", new ArrayList<String>());
        record.put("colorsId", new ArrayList<String>());

        record.put("fabricsTr", new ArrayList<String>());
        record.put("fabricsEn", new ArrayList<String>());
        record.put("fabricsFr", new ArrayList<String>());
        record.put("fabricsDe", new ArrayList<String>());
        record.put("fabricsAr", new ArrayList<String>());
        record.put("fabricsFa", new ArrayList<String>());
        record.put("fabricsId", new ArrayList<String>());

        return record;
    }
}

class ProductImages {

    private static final Schema PRODUCT_MODEL_SCHEMA_FOR_JOIN = SchemaBuilder
            .record("TempProductPricesModel")
            .fields()
            .requiredLong("productId")
            .name("productImages").type().array().items(ProductImage.PRODUCT_IMAGE_SCHEMA).noDefault()
            .endRecord();

    public static GenericRecord createEmptyRecord() {
        var record = new GenericData.Record(PRODUCT_MODEL_SCHEMA_FOR_JOIN);
        record.put("productId", 0L);
        record.put("productImages", new ArrayList<>());
        return record;
    }
}




