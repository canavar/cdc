set -x
set -m

/entrypoint.sh couchbase-server &

sleep 15

couchbase-cli cluster-init \
    --cluster-name cdc \
    --cluster-username admin \
    --cluster-password 123123 \
    --services data,index,query,fts,eventing

couchbase-cli bucket-create \
    -c 127.0.0.1:8091 \
    -u admin \
    -p 123123 \
    --bucket products \
    --bucket-type couchbase \
    --bucket-ramsize 384

fg 1