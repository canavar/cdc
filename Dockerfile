FROM adoptopenjdk/openjdk14:alpine-slim
WORKDIR /app/
COPY target/sherlock-cdc.jar .
ENTRYPOINT ["java", "-jar", "sherlock-cdc.jar"]